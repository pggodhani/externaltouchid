package cordova.plugin.external.touch.id;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class externalTouchId extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        }

        if(action.equals("fnAdd")){
            this.fnAdd ( args ,callbackContext );
            return true;
        }
        return false;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

   private void fnAdd(JSONArray args, CallbackContext callback) {
  callback.success(args);
  /*
       if(args != null) {
          try {


               int p1 = Integer.parseInt(args.getJSONObject ( 0 ).getString ( "p1" ));
               int p2 = Integer.parseInt(args.getJSONObject ( 0 ).getString ( "p2" ));
                 
             }
             
             catch (Exception e) { 

              callback.error(e);
             }
          
       }
       else {
           callback.error("Please do not pass null value");
       }
       */
   }
}
